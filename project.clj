(defproject reddit-clone "0.1.0"
  :description "Clone of Reddit, written in Clojure."
  :url "https://gitlab.com/salohonka/reddit-clone"

  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring "1.9.3"]
                 [hikari-cp "2.13.0"]
                 [org.postgresql/postgresql "42.2.20.jre7"]
                 [com.layerware/hugsql "0.5.1"]
                 [metosin/reitit "0.5.13"]
                 [metosin/malli "0.5.1"]
                 [metosin/muuntaja "0.6.8"]
                 [migratus "1.3.5"]
                 [environ "1.2.0"]
                 [org.slf4j/slf4j-simple "1.7.28"]
                 [com.stuartsierra/component "1.0.0"]
                 [buddy/buddy-auth "3.0.1"]
                 [nano-id "1.0.0"]
                 [hugsql-adapter-case "0.1.0"]
                 [clojure.java-time "0.3.2"]]

  :plugins [[migratus-lein "0.7.3"]
            [lein-environ "1.2.0"]]

  :migratus {:store         :database
             :migration-dir "migrations"
             ; TODO: Figure smart way to create reddit db and user before migratus is run.
             :db            ~(str
                               "postgresql://"
                               (get (System/getenv) "DATABASE_URL" "localhost")
                               "/" (get (System/getenv) "DATABASE_NAME" "reddit")
                               "?user=" (get (System/getenv) "POSTGRES_USER" "reddit")
                               "&password=" (get (System/getenv) "POSTGRES_PASSWORD" ""))}

  :source-paths ["src/clj"]
  :resource-paths ["src/clj" "profiles.clj" "resources"]
  :main ^:skip-aot reddit-clone.core
  :target-path "target/%s"
  :aliases {"kaocha" ["with-profile" "test" "run" "-m" "kaocha.runner"]})
