ALTER TABLE posts
    ADD COLUMN title VARCHAR(200)
        NOT NULL DEFAULT 'missing title';
