ALTER TABLE posts
    RENAME COLUMN post_id TO id;
--;;
ALTER TABLE comments
    RENAME COLUMN comment_id TO id;
