ALTER TABLE posts
    RENAME COLUMN id TO post_id;
--;;
ALTER TABLE comments
    RENAME COLUMN id TO comment_id;
