CREATE TABLE IF NOT EXISTS users
(
    name     VARCHAR(24) NOT NULL PRIMARY KEY,
    password TEXT        NOT NULL
);
