ALTER TABLE post_votes
    ADD CONSTRAINT unique_owner_post
        UNIQUE (owner, post);
--;;
ALTER TABLE comment_votes
    ADD CONSTRAINT unique_owner_comment
        UNIQUE (owner, comment);
