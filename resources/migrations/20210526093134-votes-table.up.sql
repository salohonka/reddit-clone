CREATE TABLE IF NOT EXISTS post_votes
(
    owner  VARCHAR(24) NOT NULL REFERENCES users ON DELETE RESTRICT,
    post   VARCHAR(10) NOT NULL REFERENCES posts ON DELETE CASCADE,
    upvote BOOLEAN DEFAULT TRUE
);
--;;
CREATE TABLE IF NOT EXISTS comment_votes
(
    owner   VARCHAR(24) NOT NULL REFERENCES users ON DELETE RESTRICT,
    comment VARCHAR(10) NOT NULL REFERENCES comments ON DELETE CASCADE,
    upvote  BOOLEAN DEFAULT TRUE
);
