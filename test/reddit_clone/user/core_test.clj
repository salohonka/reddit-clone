(ns reddit-clone.user.core-test
  (:require [clojure.test :refer :all]
            [reddit-clone.test-util :as util]
            [muuntaja.core :as m]))

(deftest create-user
  (is (= 201
         (do (util/drop-users!)
             (:status (util/create-user! "u-test-1"))))
      "Success creating user")

  (is (= 400
         (do (util/drop-users!)
             (util/create-user! "u-test-2")
             (:status (util/create-user! "u-test-2"))))
      "Fail creating duplicate user"))

(deftest authenticate
  (util/drop-users!)
  (util/create-user! "u-test-3")                            ; Doesn't matter if fails

  (testing "Successful login"
    (let [response (util/login! "u-test-3" "password")]
      (is (= 200 (:status response)) "Status")
      (is (some? (-> response
                     m/decode-response-body
                     :token))
          "Token")))

  (is (= 401 (:status (util/login! "u-test-3" "wrong")))
      "Unsuccessful login"))

(deftest remove-user
  (util/drop-users!)
  (util/create-user! "u-test-4")
  (util/create-user! "u-test-5")
  (reset! util/auth-token (util/get-auth-token "u-test-4"))

  (is (= 403
         (:status (util/test-route "/api/users/u-test-4"
                                   :delete
                                   {:headers [{:header "authorization"
                                               :value  "Token wrong"}]})))
      "Incorrect token")

  (is (= 403
         (:status (util/test-route
                    "/api/users/u-test-5"
                    :delete
                    {:headers [(util/get-auth-header)]})))
      "Incorrect user")

  (is (= 400
         (:status (util/test-route "/api/users/u-test-5" :delete)))
      "Missing token")

  (is (= 204
         (:status (util/test-route
                    "/api/users/u-test-4"
                    :delete
                    {:headers [(util/get-auth-header)]})))
      "Success"))

(deftest refresh-authentication
  (testing "Success"
    (util/drop-users!)
    (util/create-user! "u-test-6")
    (reset! util/auth-token (util/get-auth-token "u-test-6"))
    (let [response (util/test-route
                     "/api/refresh"
                     :get
                     {:headers [(util/get-auth-header)]})]
      (is (= 200 (:status response)) "Status")
      (is (some? (-> response
                     m/decode-response-body
                     :token))
          "Token received")))

  (testing "Unauthorized fail"
    (is (= 403
           (:status (util/test-route
                      "/api/refresh"
                      :get
                      {:headers [{:header "authorization"
                                  :value  "Token wrong"}]})))
        "Status")))
