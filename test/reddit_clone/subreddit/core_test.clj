(ns reddit-clone.subreddit.core-test
  (:require [clojure.test :refer :all]
            [reddit-clone.test-util :as util]))

(defn- setup [name]
  (util/drop-subreddits!)
  (util/create-user! name)
  (reset! util/auth-token (util/get-auth-token name)))

(deftest create-subreddit
  (is (= 201
         (do (setup "sr-test-1")
             (:status (util/create-subreddit! "sr-test-1"))))
      "Success creating subreddit")

  (is (= 400
         (do (setup "sr-test-2")
             (util/create-subreddit! "sr-test-2")
             (:status (util/create-subreddit! "sr-test-2"))))
      "Fail creating duplicate subreddit"))

(deftest remove-subreddit
  (is (= 204
         (do (setup "sr-test-3")
             (util/create-subreddit! "sr-test-3")
             (:status (util/remove-subreddit! "sr-test-3"))))
      "Success removing subreddit")

  (is (= 404
         (do (setup "sr-test-4")
             (:status (util/remove-subreddit! "sr-test-4"))))
      "Fail removing nonexistent subreddit")

  (is (= 403
         (do (setup "sr-test-5-1")
             (util/create-subreddit! "sr-test-5")
             (util/create-user! "sr-test-5-2")
             (:status (util/remove-subreddit!
                        "sr-test-5"
                        (util/get-auth-token "sr-test-5-2")))))
      "Fail removing not owned subreddit"))

(deftest get-subreddit-info
  (is (= 200
         (do (setup "sr-test-6")
             (util/create-subreddit! "sr-test-6")
             (:status (util/test-route "/api/subreddits/sr-test-6" :get))))
      "Success querying subreddit info")

  (is (= 404
         (do (setup "sr-test-7")
             (util/create-subreddit! "sr-test-7")
             (:status (util/test-route "/api/subreddits/wrong" :get))))
      "Success querying subreddit info"))
