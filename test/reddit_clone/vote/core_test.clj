(ns reddit-clone.vote.core-test
  (:require [clojure.test :refer :all]
            [reddit-clone.test-util :as util]))

(defn- setup [name]
  (util/drop-comment-votes!)
  (util/drop-post-votes!)
  (util/drop-comments!)
  (util/drop-posts!)
  (util/drop-subreddits!)
  (util/drop-users!)
  (util/create-user! name)
  (reset! util/auth-token (util/get-auth-token name))
  (util/create-subreddit! name)
  (let [post (-> {:subreddit name
                  :content   (str name " post content")
                  :title     (str name " title")}
                 util/create-post!
                 util/extract-created-id)
        comment (-> {:content (str name " comment content")
                     :post    post}
                    util/create-comment!
                    util/extract-created-id)]
    {:post post, :comment comment}))

(deftest vote-post
  (is (= 204
         (:status (util/test-route
                    (str "/api/posts/"
                         (:post (setup "v-test-1"))
                         "/vote")
                    :post
                    {:body    {:upvote true}
                     :headers [(util/get-auth-header)]})))
      "Success returns 204")

  (is (= 404
         (:status (util/test-route
                    "/api/posts/wrong67890/vote"
                    :post
                    {:body    {:upvote true}
                     :headers [(util/get-auth-header)]})))
      "Post not found returns 404")

  (is (= 400
         (:status (util/test-route
                    (str "/api/posts/"
                         (:post (setup "v-test-2"))
                         "/vote")
                    :post
                    {:headers [(util/get-auth-header)]})))
      "Missing params returns 400")

  (is (= 403
         (:status (util/test-route
                    (str "/api/posts/"
                         (:post (setup "v-test-3"))
                         "/vote")
                    :post
                    {:body    {:upvote true}
                     :headers [{:header "authorization"
                                :value  "Token wrong"}]})))
      "Unauthorized returns 403"))

(deftest vote-comment
  (is (= 204
         (:status (util/test-route
                    (str "/api/comments/"
                         (:comment (setup "v-test-4"))
                         "/vote")
                    :post
                    {:body    {:upvote true}
                     :headers [(util/get-auth-header)]})))
      "Success returns 204")

  (is (= 404
         (:status (util/test-route
                    "/api/comments/wrong67890/vote"
                    :post
                    {:body    {:upvote true}
                     :headers [(util/get-auth-header)]})))
      "Comment not found returns 404")

  (is (= 400
         (:status (util/test-route
                    (str "/api/comments/"
                         (:comment (setup "v-test-5"))
                         "/vote")
                    :post
                    {:headers [(util/get-auth-header)]})))
      "Missing params returns 400")

  (is (= 403
         (:status (util/test-route
                    (str "/api/comments/"
                         (:comment (setup "v-test-6"))
                         "/vote")
                    :post
                    {:body    {:upvote true}
                     :headers [{:header "authorization"
                                :value  "Token wrong"}]})))
      "Unauthorized returns 403"))

(deftest remove-vote-post
  (testing "Success"
    (let [{:keys [post]} (setup "v-test-7")]
      (is (= 204 (:status (util/vote-on-post! post true)))
          "Initial vote status")
      (is (= 204
             (:status (util/test-route
                        (str "/api/posts/"
                             post
                             "/vote")
                        :delete
                        {:headers [(util/get-auth-header)]})))
          "Vote removed status")))

  (is (= 404
         (:status (util/test-route
                    "/api/posts/wrong67890/vote"
                    :delete
                    {:headers [(util/get-auth-header)]})))
      "Post not found returns 404")

  (is (= 403
         (:status (util/test-route
                    (str "/api/posts/"
                         (:post (setup "v-test-8"))
                         "/vote")
                    :delete
                    {:headers [{:header "authorization"
                                :value  "Token wrong"}]})))
      "Unauthorized returns 403"))

(deftest remove-vote-comment
  (testing "Success"
    (let [{:keys [comment]} (setup "v-test-9")]
      (is (= 204 (:status (util/vote-on-comment! comment true)))
          "Initial vote status")
      (is (= 204
             (:status (util/test-route
                        (str "/api/comments/"
                             comment
                             "/vote")
                        :delete
                        {:headers [(util/get-auth-header)]})))
          "Vote removed status")))

  (is (= 404
         (:status (util/test-route
                    "/api/comments/wrong67890/vote"
                    :delete
                    {:headers [(util/get-auth-header)]})))
      "Post not found returns 404")

  (is (= 403
         (:status (util/test-route
                    (str "/api/comments/"
                         (:comment (setup "v-test-10"))
                         "/vote")
                    :delete
                    {:headers [{:header "authorization"
                                :value  "Token wrong"}]})))
      "Unauthorized returns 403"))

#_(deftest get-user-votes-for-posts)

#_(deftest get-user-votes-for-comments)

;; get-posts-votes and get-comments-votes are implicitly
;; included in the posts and comments tests. See
;; reddit-clone.test-util/generate-posts-test and
;; reddit-clone.comment.core-test
