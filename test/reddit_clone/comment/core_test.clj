(ns reddit-clone.comment.core-test
  (:require [clojure.test :refer :all]
            [reddit-clone.test-util :as util]
            [muuntaja.core :as m]))

(defn- setup [name]
  (util/drop-comments!)
  (util/drop-posts!)
  (util/drop-subreddits!)
  (util/drop-users!)
  (util/create-user! name)
  (reset! util/auth-token (util/get-auth-token name))
  (util/create-subreddit! name)
  (-> {:subreddit name
       :content   (str name " content")
       :title     (str name " title")}
      util/create-post!
      util/extract-created-id))

(defn- get-user-comment-page [user page]
  (util/test-route
    (str "/api/users/" user "/comments?page=" page)
    :get))

(deftest create-comment
  (testing "Success root comment"
    (let [post-id (setup "c-test-1")
          response (util/test-route "/api/comments"
                                    :post
                                    {:headers [(util/get-auth-header)]
                                     :body    {:content "content"
                                               :post    post-id}})]
      (is (= 201 (:status response)) "Status")
      (is (some? (get-in response [:headers "Location"])))))

  (testing "Success child comment"
    (let [post-id (setup "c-test-2")
          parent-id (-> (util/create-comment! {:content "content"
                                               :post    post-id})
                        util/extract-created-id)
          response (util/test-route "/api/comments"
                                    :post
                                    {:headers [(util/get-auth-header)]
                                     :body    {:content "content"
                                               :parent  parent-id
                                               :post    post-id}})
          child-id (util/extract-created-id response)]
      (is (= 201 (:status response)) "Status")
      (is (some? child-id) "Child comment id created")
      (is (not= child-id parent-id) "Different comment ids")))

  (testing "Unauthorized fail"
    (is (= 403
           (let [post-id (setup "c-test-3")]
             (:status (util/test-route
                        "/api/comments"
                        :post
                        {:headers [{:header "authorization"
                                    :value  "Token wrong"}]
                         :body    {:content "content"
                                   :post    post-id}}))))
        "Status")))

(deftest remove-comment
  (is (= 404
         (do (setup "c-test-4")
             (:status (util/test-route
                        "/api/comments/wrong67890"
                        :delete
                        {:headers [(util/get-auth-header)]}))))
      "Post not found")

  (is (= 403
         (let [post-id (setup "c-test-5")]
           (util/create-user! "c-test-5-1")
           (:status (util/test-route
                      (str "/api/comments/"
                           (-> {:post    post-id
                                :content "content"}
                               util/create-comment!
                               util/extract-created-id))
                      :delete
                      {:headers [{:header "authorization"
                                  :value  (str "Token "
                                               (util/get-auth-token
                                                 "c-test-5-1"))}]}))))
      "Requester not owner")

  (is (= 204
         (let [post-id (setup "c-test-6")]
           (:status (util/test-route
                      (str "/api/comments/"
                           (-> {:post    post-id
                                :content "content"}
                               util/create-comment!
                               util/extract-created-id))
                      :delete
                      {:headers [(util/get-auth-header)]}))))
      "Post successfully removed"))

(deftest get-comment
  (testing "Success"
    (let [comment-id (-> {:content "c"
                          :post    (setup "c-test-7")}
                         util/create-comment!
                         util/extract-created-id)
          response (util/test-route (str "/api/comments/" comment-id)
                                    :get)]
      (is (= 200 (:status response)) "Status")
      (is (= "c" (-> response
                     m/decode-response-body
                     :content))
          "Contents")))

  (testing "Removed-censored success"
    (let [comment-id (-> {:content "c"
                          :post    (setup "c-test-8")}
                         util/create-comment!
                         util/extract-created-id)
          response (delay (util/test-route
                            (str "/api/comments/" comment-id)
                            :get))]
      (util/test-route
        (str "/api/comments/" comment-id)
        :delete
        {:headers [(util/get-auth-header)]})

      (is (= 200 (:status @response)) "Status")
      (is (nil? (-> @response
                    m/decode-response-body
                    :poster))
          "Contents")))

  (testing "Not found fail"
    (setup "c-test-9")
    (is (= 404 (:status (util/test-route
                          "/api/comments/wrong67890"
                          :get)))
        "Status")))

(deftest update-comment
  (testing "Success"
    (let [post-id (setup "c-test-10")
          comment-id (-> {:post    post-id
                          :content "content"}
                         util/create-comment!
                         util/extract-created-id)
          response (util/test-route
                     (str "/api/comments/" comment-id)
                     :put
                     {:headers [(util/get-auth-header)]
                      :body    {:content "new content"}})]
      (is (= 204 (:status response)) "Status")
      (is (= "new content"
             (-> (util/test-route (str "/api/comments/" comment-id)
                                  :get)
                 m/decode-response-body
                 :content)))))

  (testing "Not author fail"
    (let [post-id (setup "c-test-11")
          comment-id (-> {:post    post-id
                          :content "content"}
                         util/create-comment!
                         util/extract-created-id)
          response (util/test-route
                     (str "/api/comments/" comment-id)
                     :put
                     {:headers [{:header "authorization"
                                 :value  (util/get-auth-token
                                           "c-test-12")}]
                      :body    {:content "new content"}})]
      (is (= 403 (:status response)) "Status")))

  (testing "Not found fail"
    (util/create-comment! {:content "content"
                           :post    (setup "c-test-12")})
    (is (= 404 (:status (util/test-route
                          "/api/comments/wrong67890"
                          :put
                          {:headers [(util/get-auth-header)]
                           :body    {:content "new content"}})))
        "Status")))

(deftest get-comment-children
  (testing "Success, full chain"
    (let [post-id (setup "c-test-13")
          created-comments (util/create-comment-chain!
                             post-id
                             10)
          response (util/test-route
                     (str "/api/comments/"
                          (first created-comments)
                          "/comments")
                     :get)
          fetched-comments (-> response
                               m/decode-response-body)]
      (is (= 10 (count created-comments)) "Comments created")
      (is (= 200 (:status response)) "Status")
      (is (= (->> created-comments
                  (drop 1)
                  (take 7)
                  set)
             (set (map :comment-id fetched-comments)))
          "Correct comments are fetched")))

  (testing "Success, partial chain"
    (let [post-id (setup "c-test-14")
          created-comments (util/create-comment-chain!
                             post-id
                             4)
          response (util/test-route
                     (str "/api/comments/"
                          (first created-comments)
                          "/comments")
                     :get)
          fetched-comments (-> response
                               m/decode-response-body)]
      (is (= 4 (count created-comments)) "Comments created")
      (is (= 200 (:status response)) "Status")
      (is (= 3 (count fetched-comments))
          "Correct amount of comments in response")
      (is (= (set (drop 1 created-comments))
             (set (map :comment-id fetched-comments)))
          "Correct comments are fetched")))

  (testing "Success, no chain"
    (let [post-id (setup "c-test-15")
          created-comment (util/extract-created-id
                            (util/create-comment!
                              {:content "content string"
                               :post    post-id}))
          response (util/test-route
                     (str "/api/comments/"
                          created-comment
                          "/comments")
                     :get)]
      (is (= 200 (:status response)) "Status")
      (is (= [] (-> response
                    m/decode-response-body))
          "Empty response body")))

  (testing "Not found"
    (is (= 404 (:status (util/test-route
                          "/api/comments/wrong67890/comments"
                          :get)))
        "Status")))

(deftest get-user-comments
  (testing "Success"
    (let [post-id (setup "c-test-16")
          page-1-resp (delay (get-user-comment-page
                               "c-test-16"
                               0))
          page-2-resp (delay (get-user-comment-page
                               "c-test-16"
                               1))
          page-3-resp (delay (get-user-comment-page
                               "c-test-16"
                               2))
          page-1-body (delay (m/decode-response-body
                               @page-1-resp))
          page-2-body (delay (m/decode-response-body
                               @page-2-resp))
          page-3-body (delay (m/decode-response-body
                               @page-3-resp))]
      (doseq [x (range 15)]
        (util/extract-created-id
          (util/create-comment!
            {:content (str "comment " x)
             :post    post-id})))
      (is (= 200 (:status @page-1-resp)) "Status page 1")
      (is (= 200 (:status @page-2-resp)) "Status page 2")
      (is (= 200 (:status @page-3-resp)) "Status page 3")
      (is (= 10 (count @page-1-body)) "Response body 1st page")
      (is (= 5 (count @page-2-body)) "Response body 2nd page")
      (is (= 0 (count @page-3-body)) "Response body 3rd page")
      (is (every? :votes (concat @page-1-body
                                 @page-2-body
                                 @page-3-body))
          "Votes get counted")))

  (is (= 404 (:status (util/test-route
                        "/api/users/wrong/comments"
                        :get)))
      "Not found returns 404"))

(deftest get-post-comments
  (testing "Success, full chain"
    (let [post-id (setup "c-test-17")
          created-comments (util/create-comment-chain!
                             post-id
                             10)
          response (util/test-route
                     (str "/api/posts/"
                          post-id
                          "/comments")
                     :get)
          fetched-comments (-> response
                               m/decode-response-body)]
      (is (= 10 (count created-comments)) "Comments created")
      (is (= 200 (:status response)) "Status")
      (is (= (set (take 8 created-comments))
             (set (map :comment-id fetched-comments)))
          "Correct comments are fetched")
      (is (every? :votes fetched-comments) "Votes get counted")))

  (testing "Success, partial chain"
    (let [post-id (setup "c-test-18")
          created-comments (util/create-comment-chain!
                             post-id
                             4)
          response (util/test-route
                     (str "/api/posts/"
                          post-id
                          "/comments")
                     :get)
          fetched-comments (-> response
                               m/decode-response-body)]
      (is (= 4 (count created-comments)) "Comments created")
      (is (= 200 (:status response)) "Status")
      (is (= 4 (count fetched-comments))
          "Correct amount of comments in response")
      (is (= (set created-comments)
             (set (map :comment-id fetched-comments)))
          "Correct comments are fetched")
      (is (every? :votes fetched-comments) "Votes get counted")))

  (testing "Success, no chain"
    (let [post-id (setup "c-test-19")
          response (delay (util/test-route
                            (str "/api/posts/"
                                 post-id
                                 "/comments")
                            :get))]
      (util/extract-created-id
        (util/create-comment!
          {:content "content string"
           :post    post-id}))
      (is (= 200 (:status @response)) "Status")
      (is (= 1 (-> @response
                   m/decode-response-body
                   count))
          "Correct amount of comments in response")))

  (is (= 404 (:status (util/test-route
                        "/api/posts/wrong67890/comments"
                        :get)))
      "Post not found returns 404"))
