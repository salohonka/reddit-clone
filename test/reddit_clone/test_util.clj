(ns reddit-clone.test-util
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [reddit-clone.server :refer [wrapped-app]]
            [reddit-clone.db :as db]
            [reddit-clone.user.db :as user-db]
            [reddit-clone.subreddit.db :as subreddit-db]
            [reddit-clone.post.db :as post-db]
            [reddit-clone.comment.db :as comment-db]
            [reddit-clone.vote.db :as vote-db]
            [environ.core :refer [env]]
            [hikari-cp.core :as pool]
            [reddit-clone.auth :as auth]
            [muuntaja.core :as m]
            [clojure.string :as s]
            [reddit-clone.util :as util]))

(def test-pool (atom nil))
(def auth-token (atom nil))

(defn init-test-db! [arg]
  (reset! test-pool {:pool {:datasource (pool/make-datasource
                                          (db/datasource-options
                                            (env :database-url)
                                            (env :database-name)
                                            (env :database-port)
                                            (env :postgres-user)
                                            (env :postgres-password)))}})
  arg)

(defn test-route
  ([route method]
   (test-route route method {}))

  ([route method {:keys [body headers]}]
   (let [apply-headers (fn [req xs]
                         (reduce
                           #(mock/header %1 (:header %2) (:value %2))
                           req
                           xs))]
     ((wrapped-app @test-pool)
      (cond-> (mock/request method route)
              body (mock/json-body body)
              headers (apply-headers headers))))))

(defn drop-users! []
  (user-db/drop-users (:pool @test-pool)))

(defn create-user! [name]
  (test-route
    (str "/api/users")
    :post
    {:body {:username name
            :password "password"}}))

(defn login! [name password]
  (test-route "/api/login"
              :post
              {:body {:username name
                      :password password}}))

(defn get-auth-token [name]
  (auth/generate-jwt-token name))

(defn get-auth-header []
  {:header "authorization"
   :value  (str "Token " @auth-token)})

(defn drop-subreddits! []
  (subreddit-db/drop-subreddits (:pool @test-pool)))

(defn create-subreddit!
  ([name]
   (create-subreddit! name @auth-token))

  ([name token]
   (test-route
     "/api/subreddits"
     :post
     {:body    {:subreddit name}
      :headers [{:header "authorization"
                 :value  (str "Token " token)}]})))

(defn remove-subreddit!
  ([name]
   (remove-subreddit! name @auth-token))

  ([name token]
   (test-route
     (str "/api/subreddits/" name)
     :delete
     {:headers [{:header "authorization"
                 :value  (str "Token " token)}]})))

(defn create-post!
  "Expects keys :subreddit :content :title in body."
  ([body]
   (create-post! body @auth-token))

  ([body token]
   (test-route
     "/api/posts"
     :post
     {:body    body
      :headers [{:header "authorization"
                 :value  (str "Token " token)}]})))

(defn- generate-titles [count]
  (->> (range count) (map #(str "post " %))))

(defn create-posts!
  ([count test-name]
   (doseq [title (generate-titles count)]
     (create-post! {:subreddit test-name
                    :content   "content"
                    :title     title})))

  ([count test-name token]
   (doseq [title (generate-titles count)]
     (create-post! {:subreddit test-name
                    :content   "content"
                    :title     title}
                   token))))

(defn drop-posts! []
  (post-db/drop-posts (:pool @test-pool)))

(defn extract-created-id [res]
  (-> (get-in res [:headers "Location"])
      (s/split #"/")
      last))

(defmacro generate-posts-test [name test-var-string url-start url-ending size-check]
  `(deftest ~name
     (drop-posts!)
     (drop-subreddits!)
     (drop-users!)
     (create-user! ~test-var-string)
     (reset! auth-token (get-auth-token ~test-var-string))
     (create-subreddit! ~test-var-string)
     (create-posts! 13 ~test-var-string)

     (let [response# (test-route
                       ~(str url-start test-var-string url-ending)
                       :get)
           body# (m/decode-response-body response#)]
       (is (= 200
              (:status response#))
           "Success status")

       (is (= ~size-check (count body#))
           "Query returns correct amount of posts")

       (is (every? :votes body#)
           "Vote counts get added to posts"))))

(defn drop-comments! []
  (comment-db/drop-comments (:pool @test-pool)))

(defn create-comment!
  "Expects keys :content :post :parent in body."
  ([body]
   (create-comment! body @auth-token))

  ([body token]
   (test-route
     "/api/comments"
     :post
     {:body    body
      :headers [{:header "authorization"
                 :value  (str "Token " token)}]})))

(defn create-comment-chain!
  ([post-id chain-depth]
   (create-comment-chain! post-id chain-depth @auth-token))

  ([post-id chain-depth token]
   (let [initial-comment (extract-created-id
                           (create-comment!
                             {:content (util/rand-str 10)
                              :post    post-id}
                             token))]
     (loop [iter 1
            comment-ids [initial-comment]]
       (if (= iter chain-depth)
         comment-ids
         (recur (inc iter)
                (conj comment-ids
                      (extract-created-id
                        (create-comment!
                          {:content (util/rand-str 10)
                           :post    post-id
                           :parent  (last comment-ids)}
                          token)))))))))

(defn drop-comment-votes! []
  (vote-db/drop-comment-votes (:pool @test-pool)))

(defn drop-post-votes! []
  (vote-db/drop-post-votes (:pool @test-pool)))

(defn vote-on-post!
  ([post-id upvote?]
   (vote-on-post! post-id upvote? @auth-token))

  ([post-id upvote? auth-token]
   (test-route
     (str "/api/posts/"
          post-id
          "/vote")
     :post
     {:body    {:upvote upvote?}
      :headers [{:header "authorization"
                 :value  (str "Token " auth-token)}]})))

(defn vote-on-comment!
  ([comment-id upvote?]
   (vote-on-comment! comment-id upvote? @auth-token))

  ([comment-id upvote? auth-token]
   (test-route
     (str "/api/comments/"
          comment-id
          "/vote")
     :post
     {:body    {:upvote upvote?}
      :headers [{:header "authorization"
                 :value  (str "Token " auth-token)}]})))
