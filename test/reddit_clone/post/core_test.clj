(ns reddit-clone.post.core-test
  (:require [clojure.test :refer :all]
            [reddit-clone.test-util :as util]
            [muuntaja.core :as m]))

(defn- setup [name]
  (util/drop-posts!)
  (util/drop-subreddits!)
  (util/drop-users!)
  (util/create-user! name)
  (reset! util/auth-token (util/get-auth-token name))
  (util/create-subreddit! name))

(deftest create-post
  (testing "Success"
    (setup "p-test-1")
    (let [response (util/test-route "/api/posts"
                                    :post
                                    {:headers [(util/get-auth-header)]
                                     :body    {:content   "content"
                                               :title     "title"
                                               :subreddit "p-test-1"}})]
      (is (= 201 (:status response)) "Status")
      (is (some? (get-in response [:headers "Location"])))))

  (testing "Subreddit not found fail"
    (is (= 400
           (do (setup "p-test-17")
               (:status (util/test-route
                          "/api/posts"
                          :post
                          {:headers [(util/get-auth-header)]
                           :body    {:content   "content"
                                     :title     "title"
                                     :subreddit "wrong"}}))))
        "Status"))


  (testing "Unauthorized fail"
    (is (= 403
           (do (setup "p-test-2")
               (:status (util/test-route
                          "/api/posts"
                          :post
                          {:headers [{:header "authorization"
                                      :value  "Token wrong"}]
                           :body    {:content   "content"
                                     :title     "title"
                                     :subreddit "p-test-2"}}))))
        "Status")))

(deftest remove-post
  (is (= 404
         (do (setup "p-test-3")
             (:status (util/test-route "/api/posts/wrong67890"
                                       :delete
                                       {:headers [(util/get-auth-header)]}))))
      "Post not found")

  (is (= 403
         (do (setup "p-test-4")
             (util/create-user! "p-test-4-1")
             (:status (util/test-route
                        (str "/api/posts/"
                             (-> {:title     "title"
                                  :subreddit "p-test-4"
                                  :content   "content"}
                                 util/create-post!
                                 util/extract-created-id))
                        :delete
                        {:headers [{:header "authorization"
                                    :value  (str "Token "
                                                 (util/get-auth-token
                                                   "p-test-4-1"))}]}))))
      "Requester not owner")

  (is (= 204
         (do (setup "p-test-5")
             (:status (util/test-route
                        (str "/api/posts/"
                             (-> {:title     "title"
                                  :subreddit "p-test-5"
                                  :content   "content"}
                                 util/create-post!
                                 util/extract-created-id))
                        :delete
                        {:headers [(util/get-auth-header)]}))))
      "Post successfully removed"))

(deftest get-post
  (testing "Success"
    (setup "p-test-6")
    (let [post-id (-> (util/create-post! {:title     "title-string"
                                          :content   "content-string"
                                          :subreddit "p-test-6"})
                      util/extract-created-id)
          response (util/test-route (str "/api/posts/" post-id)
                                    :get)]
      (is (= 200 (:status response)) "Status")
      (is (= {:post-id   post-id, :poster "p-test-6", :removed false
              :subreddit "p-test-6", :title "title-string"}
             (-> response
                 m/decode-response-body
                 (select-keys [:post-id :poster :subreddit :title :removed])))
          "Contents"))))

(deftest get-user-posts-not-found
  (is (= 404
         (do (setup "p-test-7")
             (util/create-subreddit! "p-test-7")
             (util/create-posts! 2 "p-test-7")
             (:status (util/test-route
                        "/api/users/wrong/posts"
                        :get))))
      "Fail on not finding user"))

(util/generate-posts-test get-user-posts-no-arg
                          "p-test-8"
                          "/api/users/"
                          "/posts"
                          10)

(util/generate-posts-test get-user-posts-page-arg
                          "p-test-9"
                          "/api/users/"
                          "/posts?page=1"
                          3)

(util/generate-posts-test get-user-posts-page-size-arg
                          "p-test-10"
                          "/api/users/"
                          "/posts?page-size=5"
                          5)

(util/generate-posts-test get-user-posts-both-arg
                          "p-test-11"
                          "/api/users/"
                          "/posts?page-size=5&page=1"
                          5)

(deftest get-subreddit-posts-not-found
  (is (= 404
         (do (setup "p-test-12")
             (util/create-subreddit! "p-test-12")
             (util/create-posts! 2 "p-test-12")
             (:status (util/test-route
                        "/api/subreddits/wrong/posts"
                        :get))))
      "Fail on not finding subreddit"))

(util/generate-posts-test get-subreddit-posts-no-arg
                          "p-test-13"
                          "/api/subreddits/"
                          "/posts"
                          10)

(util/generate-posts-test get-subreddit-posts-page-arg
                          "p-test-14"
                          "/api/subreddits/"
                          "/posts?page=1"
                          3)

(util/generate-posts-test get-subreddit-posts-page-size-arg
                          "p-test-15"
                          "/api/subreddits/"
                          "/posts?page-size=5"
                          5)

(util/generate-posts-test get-subreddit-posts-both-arg
                          "p-test-16"
                          "/api/subreddits/"
                          "/posts?page-size=5&page=1"
                          5)
