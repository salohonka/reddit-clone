# reddit-clone

Clojure copycat of Reddit. Built on Metosin stack (Reitit, Malli, Muuntaja), Postgres + hikari-cp + hugsql + migratus,
component, buddy, environ. A Swagger/OpenAPI explorer is built into the project, and the REST API can explored
at `/docs` on a running server.

Backend only for now.

## Running

### With Docker

#### Docker-compose

1. Make sure postgres is not running
2. Edit `.env` to contain your Postgres password (POSTGRES_PASSWORD)
3. `docker-compose up`
4. Navigate to [localhost:3000/docs](http://localhost:3000/docs)

#### Docker

1. `docker run salohonka/reddit-clone:latest`

Remember to set your environment variables, especially database related variables, as the defaults are made for
docker-compose.

### Manually running the jar

1. Build jar with `lein uberjar`
2. Copy the produced jar from `target/uberjar/reddit-clone.jar`
3. Apply `resources/sql/init.sql` to your postgres instance
4. Set your environment variables
4. `java -jar reddit-clone.jar`

### Environment variables

The application server has several mandatory environment variables. These are set in the docker image with defaults, but
can be overwritten. For testing and development, the leiningen profiles contain defaults. `.env` file should be edited
to hold the password for Postgres if launching with Docker-compose.

| Variable name     | Dev default     | Prod default (docker)          |
|-------------------|-----------------|--------------------------------|
| DATABASE_URL      | localhost       | db                             |
| DATABASE_NAME     | reddit          | reddit                         |
| DATABASE_PORT     | 5432            | 5432                           |
| POSTGRES_USER     | reddit          | reddit                         |
| POSTGRES_PASSWORD | <empty>         | <set by .env>                  |
| PORT              | 3000            | 3000                           |
| DEV               | true            | <empty> (interpreted as false) |
| JWT_SECRET        | <random string> | <set by .env>                  |

## Development

`reddit-clone.core` contains utilities to manually define, start, and stop the system, and also to extract the
ring-handler, create a debugging authentication token, and simulate a request.

### Running the development environment

1. Copy the development profile from `profiles.example.clj` or create your own
2. Start Postgres
3. Initialise the database by running `resources/sql/init.sql` and `resources/sql/init.dev.sql` on the db
4. Start your repl

## Testing

Project uses Kaocha for running Clojure tests. As recommended by the authors, a binstub is available.

The tests are integration tests that perform a ring-request on an endpoint, and the ring-response is asserted on. Test
data is stored in reddit_test database.

### Running the tests

1. Copy the testing profile from `profiles.example.clj` or create your own
2. `bin/kaocha` or `lein kaocha`
