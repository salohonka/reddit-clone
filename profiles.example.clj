{:dev     {:env          {:database-url      "localhost"
                          :database-name     "reddit"
                          :database-port     5432
                          :postgres-user     "reddit"
                          :postgres-password nil
                          :port              3000
                          :dev               true}
           :dependencies [[ring/ring-mock "0.4.0"]]}

 :test    {:env          {:database-url      "localhost"
                          :database-name     "reddit_test"
                          :database-port     5432
                          :postgres-user     "reddit_test"
                          :postgres-password nil
                          :port              3001
                          :dev               true}
           :dependencies [[lambdaisland/kaocha "1.0.861"]
                          [ring/ring-mock "0.4.0"]]}

 :uberjar {:aot          :all
           :omit-source  true
           :uberjar-name "reddit-clone.jar"}}
