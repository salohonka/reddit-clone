(ns reddit-clone.user.core
  (:require [reddit-clone.middleware :as mw]
            [reitit.coercion.malli :refer [coercion]]
            [reddit-clone.util :as util]
            [ring.util.response :as res]
            [reddit-clone.user.db :as user-db]
            [reddit-clone.auth :as auth]
            [reddit-clone.post.core :as post]
            [reddit-clone.comment.core :as comment])
  (:import (java.sql SQLException)))

(defn- create-user! [{db :db, payload :body-params, :as _req}]
  (try
    (user-db/insert-user db payload)
    (res/created (str "/users/" (:username payload)))
    (catch SQLException _
      (res/bad-request {:message "User already exists."}))))

(defn- remove-user!
  "Flags the account as deleted. Comments and
  posts made are not removed, but the author
  username is marked as \"Deleted\"."
  [{db :db, {username :username} :path-params, :as req}]
  (util/with-sql-try
    (let [changed (if (or (= username (-> req :identity :user))
                          (-> req :identity :admin))
                    (user-db/update-user-removed db {:username username
                                                     :removed  true})
                    :not-author)]
      (case changed
        1 (res/status 204)
        :not-author (res/status 403)))))

(defn- authenticate [{db :db, {:keys [username] :as payload} :body-params}]
  (util/with-sql-try
    (if (:?column? (user-db/check-user-password db payload))
      (res/response {:token (auth/generate-jwt-token username)})
      (res/status 401))))

(defn- refresh-authentication [req]
  (res/response {:token (auth/generate-jwt-token
                          (-> req :identity :user))}))

(defn- get-activity
  "Gets combined feed of posts and comments made
  by user. Results are paginated."
  [req]
  nil)

(defn- create-user-endpoint [handler]
  {:parameters {:body [:map
                       [:username [:re util/username-regex]]
                       [:password [:re util/password-regex]]]}
   :handler    handler})

(defn routes []
  ["" {:swagger {:tags ["Users"]}, :coercion coercion}
   ["/login" {:post (create-user-endpoint authenticate)}]
   ["/refresh" {:get        {:handler refresh-authentication}
                :parameters {:header util/auth-header}
                :middleware [mw/wrap-token mw/wrap-auth]}]

   ["/users"
    ["" {:post (create-user-endpoint create-user!)}]
    ["/:username"
     {:parameters {:path [:map [:username
                                [:re util/username-regex]]]}
      :middleware [mw/wrap-user-exists]}

     ["" {:delete {:handler    remove-user!
                   :parameters {:header util/auth-header}
                   :middleware [mw/wrap-token mw/wrap-auth]}}]

     ["/posts" {:get (util/create-paged-endpoint
                       post/get-user-posts)}]
     ["/comments" {:get (util/create-paged-endpoint
                          comment/get-user-comments)}]
     ["/feed" {:get (util/create-paged-endpoint
                      get-activity)}]]]])
