(ns reddit-clone.user.db
  (:require [hugsql.core :as sql]
            [hugsql-adapter-case.adapters :refer [kebab-adapter]]))

(sql/def-db-fns "reddit_clone/user/queries.sql" {:adapter (kebab-adapter)})
