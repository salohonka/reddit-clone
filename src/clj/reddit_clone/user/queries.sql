-- :name insert-user :! :n
INSERT INTO users (name, password)
VALUES (:username, crypt(:password, gen_salt('bf')));

-- :name update-user-password :! :n
UPDATE users
SET password = crypt(:password, gen_salt('bf'))
WHERE name = :username;

-- :name check-user-password :? :1
-- :doc Returns true if :password is correct for :name.
SELECT password = crypt(:password, password) AND NOT removed
FROM users
WHERE name = :username;

-- :name update-user-removed :! :n
UPDATE users
SET removed = :removed
WHERE name = :username;

-- :name check-user-removed :? :1
-- :doc Returns boolean based on if user is removed.
SELECT removed
FROM users
WHERE name = :username;

-- :name drop-users :! :n
-- :doc Removes all users from table for testing purposes
TRUNCATE users CASCADE;

-- :name get-user-info :? :1
SELECT (name, created)
FROM users
WHERE name = :name;
