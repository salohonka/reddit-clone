(ns reddit-clone.db
  (:require [hikari-cp.core :as pool]
            [com.stuartsierra.component :as component]
            [migratus.core :as migratus]
            [reddit-clone.util :as util]))

(defn migrate [datasource]
  (let [config {:db            {:datasource datasource}
                :store         :database
                :migration-dir "migrations"}]
    (migratus/init config)
    (migratus/migrate config)))

(defn datasource-options [host name port username password]
  {:pool-name     "reddit-pool"
   :adapter       "postgresql"
   :username      username
   :password      password
   :database-name name
   :server-name   host
   :port-number   (Integer/parseInt port)})

(defrecord Database [db-url db-name db-port db-user db-password pool]
  component/Lifecycle

  (start [this]
    (let [datasource (pool/make-datasource
                       (datasource-options db-url
                                           db-name
                                           db-port
                                           db-user
                                           db-password))]
      (when-not util/dev?
        (migrate datasource))
      (assoc this :pool {:datasource datasource})))

  (stop [this]
    (pool/close-datasource (-> this :pool :datasource))
    (assoc this :pool nil)))
