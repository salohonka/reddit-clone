(ns reddit-clone.server
  (:require [com.stuartsierra.component :as component]
            [ring.adapter.jetty :as jetty]
            [reitit.ring :as ring]
            [reitit.dev.pretty :as pretty]
            [ring.middleware.reload :refer [wrap-reload]]
            [reddit-clone.util :refer [dev?]]
            [reddit-clone.user.core :as user]
            [reddit-clone.subreddit.core :as subreddit]
            [reddit-clone.post.core :as post]
            [reddit-clone.comment.core :as comment]
            [reddit-clone.vote.core :as vote]
            [reitit.coercion :as coercion]
            [reitit.swagger :as swagger]
            [reitit.swagger-ui :as swagger-ui]
            [reddit-clone.middleware :as mw]
            [muuntaja.core :as m]
            [reitit.spec :as rs])
  (:import (org.eclipse.jetty.server Server)))

(defn- routes []
  [["/swagger.json" {:no-doc  true
                     :swagger {:info {:title "reddit-clone api"}}
                     :handler (swagger/create-swagger-handler)}]
   ["/api"
    (user/routes)
    (subreddit/routes)
    (post/routes)
    (comment/routes)
    (vote/routes)]])

(def router-opts {:data {:coercion   coercion/compile-request-coercers
                         :muuntaja   m/instance
                         :middleware mw/default-middleware
                         :validate   rs/validate
                         :exception  pretty/exception}})
(def ^:private prod-router (constantly (ring/router (routes) router-opts)))
(def ^:private dev-router #(ring/router (routes) router-opts))

(def ^:private app
  (ring/ring-handler
    (if dev? (dev-router) (prod-router))
    (ring/routes
      (swagger-ui/create-swagger-ui-handler {:path "/docs"})
      (ring/redirect-trailing-slash-handler {:method :strip})
      (ring/create-default-handler))))

(defn wrapped-app [db]
  (-> (if dev? (wrap-reload #'app) app)
      (mw/wrap-db db)))

(defrecord ApplicationServer [port db server]
  component/Lifecycle

  (start [this]
    (assoc this :server (jetty/run-jetty
                          (wrapped-app db)
                          {:join? false
                           :port  (Integer/parseInt port)})))

  (stop [this]
    (.stop ^Server (:server this))
    (assoc this :server nil)))
