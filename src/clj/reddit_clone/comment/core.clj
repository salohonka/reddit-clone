(ns reddit-clone.comment.core
  (:require [reddit-clone.middleware :as mw]
            [reddit-clone.util :as util]
            [reddit-clone.comment.db :as db]
            [reitit.coercion.malli :refer [coercion]]
            [ring.util.response :as res]
            [reddit-clone.vote.core :as vote]
            [clojure.set :refer [join]]))

(defn create-comment! [{:keys [db] :as req}]
  (let [{:keys [content post parent]} (-> req :body-params)
        comment-id (util/gen-id 10)]
    (db/insert-comment
      db
      {:comment_id comment-id
       :poster     (-> req :identity :user)
       :post       post
       :content    content
       :parent     parent})
    (res/created (str "/api/comments/" comment-id))))

(defn remove-comment!
  "Flag the comment as removed, displaying it as \"Removed\"
  to everybody and preventing further modification. Original
  data is preserved in db and can be queried manually."
  [{:keys [db] :as req}]
  (db/update-comment-removed
    db
    {:comment_id (-> req :path-params :comment-id)
     :removed    true})
  (res/status 204))

(defn edit-comment! [{:keys [db] :as req}]
  (db/update-comment-content
    db
    {:comment_id (-> req :path-params :comment-id)
     :content    (-> req :body-params :content)})
  (res/status 204))

(defn- purge-removed-comments [comments]
  (mapv #(cond-> %
                 (:removed %) (assoc :poster nil
                                     :content nil))
        comments))

(defn get-comment-children
  "Get child comments. Search depth is 8 children deep."
  [{:keys [db] :as req}]
  (let [comments (purge-removed-comments
                   (db/get-comment-children
                     db
                     {:comment_id (-> req :path-params :comment-id)}))
        votes (vote/get-comments-votes db comments)]
    (res/response (join (set comments) (set votes)))))

(defn get-user-comments [{:keys [db] :as req}]
  (let [comments (purge-removed-comments
                   (db/get-user-comments
                     db
                     (merge {:poster (-> req :path-params :username)}
                            (util/extract-pagination req))))
        votes (vote/get-comments-votes db comments)]
    (res/response (join (set comments) (set votes)))))

(defn get-post-comments
  "Get post comments. Search depth is 8 children deep."
  [{:keys [db] :as req}]
  (let [comments (purge-removed-comments
                   (db/get-post-root-comments
                     db
                     {:post_id (-> req :path-params :post-id)}))
        votes (vote/get-comments-votes db comments)]
    (res/response (join (set comments) (set votes)))))

(defn get-comment [{:keys [db] :as req}]
  (let [comment-id (-> req :path-params :comment-id)
        {:keys [removed] :as result} (db/get-comment
                                       db
                                       {:comment_id comment-id})
        votes (vote/get-comments-votes db [comment-id])]
    (res/response (cond-> result
                          removed (assoc :poster nil
                                         :content nil)
                          true (assoc :votes
                                      (-> votes first :votes))))))

(defn routes []
  ["" {:swagger {:tags ["Comments"]}, :coercion coercion}
   ["/comments"
    ["" {:post {:handler    create-comment!
                :parameters {:header util/auth-header
                             :body   [:map
                                      [:content [:re util/body-regex]]
                                      [:post [:re util/id-regex]]
                                      [:parent {:optional true}
                                       [:re util/id-regex]]]}
                :middleware [mw/wrap-token mw/wrap-auth]}}]

    ["/:comment-id" {:parameters {:path [:map [:comment-id
                                               [:re util/id-regex]]]}
                     :middleware [mw/wrap-comment-exists]}

     ["" {:get    {:handler get-comment}

          :delete {:handler    remove-comment!
                   :parameters {:header util/auth-header}
                   :middleware [mw/wrap-token
                                mw/wrap-auth
                                mw/wrap-comment-author]}

          :put    {:handler    edit-comment!
                   :parameters {:header util/auth-header
                                :body   [:map
                                         [:content
                                          [:re util/body-regex]]]}
                   :middleware [mw/wrap-token
                                mw/wrap-auth
                                mw/wrap-comment-author]}}]

     ["/comments" {:get {:handler get-comment-children}}]

     ["/vote" {:middleware [[mw/wrap-vote-type :comment]
                            mw/wrap-token
                            mw/wrap-auth]
               :parameters {:header util/auth-header}}
      ["" {:post   {:handler    vote/vote!
                    :parameters {:body vote/vote-params}}
           :delete {:handler vote/remove-vote!}}]]]]])
