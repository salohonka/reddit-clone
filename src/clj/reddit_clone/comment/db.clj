(ns reddit-clone.comment.db
  (:require [hugsql.core :as sql]
            [hugsql-adapter-case.adapters :refer [kebab-adapter]]))

(sql/def-db-fns "reddit_clone/comment/queries.sql" {:adapter (kebab-adapter)})
