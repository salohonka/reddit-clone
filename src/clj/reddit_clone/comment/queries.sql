-- :name drop-comments :! :n
-- :doc Removes all comments from table for testing purposes
TRUNCATE comments CASCADE;

-- :name get-post-root-comments :? :*
WITH RECURSIVE cte (comment_id,
                    poster,
                    content,
                    removed,
                    created,
                    parent,
                    depth)
                   AS (
        SELECT comment_id,
               poster,
               content,
               removed,
               created,
               parent,
               0
        FROM comments
        WHERE post = :post_id
          AND parent IS NULL

        UNION ALL

        SELECT c.comment_id,
               c.poster,
               c.content,
               c.removed,
               c.created,
               c.parent,
               depth + 1
        FROM comments c
                 INNER JOIN cte t ON c.parent = t.comment_id
        WHERE depth < 7
    )
SELECT comment_id,
       poster,
       content,
       removed,
       created,
       parent
FROM cte;

-- :name insert-comment :! :n
INSERT INTO comments (comment_id, poster, post, content, parent)
VALUES (:comment_id, :poster, :post, :content, :parent);

-- :name update-comment-removed :! :n
UPDATE comments
SET removed = :removed
WHERE comment_id = :comment_id;

-- :name update-comment-content :! :n
UPDATE comments
SET content = :content
WHERE comment_id = :comment_id;

-- :name get-comment-info :? :1
SELECT comment_id, poster, post, removed, created, parent
FROM comments
WHERE comment_id = :name;

-- :name get-comment :? :1
SELECT *
FROM comments
WHERE comment_id = :comment_id;

-- :name get-user-comments :? :*
SELECT comment_id, post, removed, created, content
FROM comments
WHERE poster = :poster
LIMIT :page_size OFFSET :page * :page_size;

-- :name get-comment-children :? :*
WITH RECURSIVE cte (comment_id,
                    poster,
                    content,
                    removed,
                    created,
                    parent,
                    depth) AS (
    SELECT comment_id,
           poster,
           content,
           removed,
           created,
           parent,
           0
    FROM comments
    WHERE comment_id = :comment_id

    UNION ALL

    SELECT c.comment_id,
           c.poster,
           c.content,
           c.removed,
           c.created,
           c.parent,
           depth + 1
    FROM comments c
             INNER JOIN cte t ON c.parent = t.comment_id
    WHERE depth < 7
)
SELECT comment_id,
       poster,
       content,
       removed,
       created,
       parent
FROM cte
WHERE comment_id != :comment_id;
