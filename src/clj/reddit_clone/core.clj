(ns reddit-clone.core
  (:gen-class)
  (:require [environ.core :refer [env]]
            [com.stuartsierra.component :as component]
            [reddit-clone.server :as server]
            [reddit-clone.db :as db]
    #_[ring.mock.request :refer [request] :as req]
    #_[reddit-clone.auth :as auth]
    #_[reddit-clone.util :as util]
    #_[muuntaja.core :as m]))

(defn create-system [config]
  (component/system-map
    :db (db/map->Database config)
    :app (component/using
           (server/map->ApplicationServer config)
           [:db])))

(defn -main
  [& _args]
  (component/start
    (create-system {:db-url      (env :database-url)
                    :db-name     (env :database-name)
                    :db-port     (env :database-port)
                    :db-user     (env :postgres-user)
                    :db-password (env :postgres-password)
                    :port        (env :port)})))

#_(comment
    (def system (create-system {:db-url      (env :database-url)
                                :db-name     (env :database-name)
                                :db-port     (env :database-port)
                                :db-user     (env :postgres-user)
                                :db-password (env :postgres-password)
                                :port        (env :port)}))

    (alter-var-root #'system component/start)
    (alter-var-root #'system component/stop)

    (def app (server/wrapped-app (-> system :db)))
    (def token (auth/generate-jwt-token "testuser"))

    (defn create-comment
      ([token post]
       (create-comment token post nil))

      ([token post parent]
       (app (-> (request :post "/api/comments")
                (req/header "authorization" (str "Token " token))
                (req/json-body (merge {:content (util/rand-str 10)
                                       :post    post}
                                      (when parent
                                        {:parent parent})))))))

    (-> (app (-> (request :get "/api/comments/JMA2OC1fOP/comments")
                 #_(req/header "authorization" (str "Token " token))
                 #_(req/json-body {:username "cmdtestuser"
                                   :password "string"})))
        m/decode-response-body))
