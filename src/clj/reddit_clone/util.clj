(ns reddit-clone.util
  (:require [environ.core :refer [env]]
            [reitit.coercion.malli :as malli-coercion]
            [ring.util.response :as res]
            [nano-id.core :refer [nano-id]])
  (:import (java.sql SQLException)))

(def dev? (Boolean/parseBoolean (env :dev)))

(defn rand-str [len]
  (apply str (take len (repeatedly #(char (+ (rand 26) 65))))))

(def default-page-size 10)

(defn create-paged-endpoint
  ([handler]
   (create-paged-endpoint handler nil))

  ([handler opts]
   (merge {:coercion   malli-coercion/coercion
           :parameters {:query [:map
                                [:page
                                 {:default  0
                                  :optional true}
                                 [:and int? [:> -1]]]
                                [:page-size
                                 {:default  default-page-size
                                  :optional true}
                                 [:and pos-int? [:<= 25]]]]}
           :handler    handler}
          opts)))

(defn- try-parse-int [s default]
  (try (Integer/parseInt s)
       (catch Exception _ default)))

(defn extract-pagination [req]
  (let [{:strs [page page-size]} (-> req :query-params)]
    {:page      (try-parse-int page 0)
     :page_size (try-parse-int page-size default-page-size)}))

(defmacro with-sql-try [expr]
  `(try ~expr
        (catch SQLException ^SQLException e#
          (-> e#
              .getMessage
              ~#(hash-map :message %)
              res/response
              (res/status 500)))))

(defn gen-id [len] (nano-id len))

(def id-regex #"^[A-Za-z\d\-_]{10}$")
(def body-regex #"^.{1,5000}$")
(def username-regex #"^[A-Za-z\d\-_]{1,15}$")
(def password-regex #"^.{1,64}$")
(def subreddit-regex #"^[A-Za-z\d\-_]{1,12}$")
(def title-regex #"^.{1,200}$")

(def auth-header [:map [:authorization string?]])
