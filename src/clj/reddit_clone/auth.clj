(ns reddit-clone.auth
  (:require [environ.core :refer [env]]
            [reddit-clone.util :as util]
            [buddy.auth.backends :as backends]
            [reddit-clone.admin.core :as admin]
            [buddy.sign.jwt :as jwt]
            [java-time :as jt]))

(def jwt-secret (or (env :jwt-secret) (util/rand-str 24)))
(def buddy-backend (backends/jws {:secret jwt-secret}))
(def expiry-days 3)

(defn- generate-jwt-payload [username]
  (let [admin? (admin/is-admin? username)]
    (merge {:user username
            :exp  (jt/plus (jt/instant)
                           (jt/days expiry-days))}
           (when admin? {:admin true}))))

(defn generate-jwt-token [username]
  (jwt/sign (generate-jwt-payload username) jwt-secret))
