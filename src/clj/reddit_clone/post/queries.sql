-- :name insert-post :! :n
INSERT INTO posts (post_id, poster, subreddit, content, title)
VALUES (:post_id, :poster, :subreddit, :content, :title);

-- :name update-post-removed :! :n
UPDATE posts
SET removed = :removed
WHERE post_id = :post_id;

-- :name get-post-info :? :1
SELECT post_id, poster, removed, subreddit, title, created
FROM posts
WHERE post_id = :name;

-- :name drop-posts :! :n
-- :doc Removes all posts from table for testing purposes
TRUNCATE posts CASCADE;

-- :name get-subreddit-posts :? :*
SELECT post_id, poster, posts.created, title
FROM posts,
     subreddits
WHERE subreddit = :subreddit
  AND subreddits.name = :subreddit
  AND subreddits.removed = FALSE
  AND subreddits.banned = FALSE
  AND posts.removed = FALSE
LIMIT :page_size OFFSET :page * :page_size;

-- :name get-user-posts :? :*
SELECT post_id, subreddit, posts.created, title
FROM posts,
     users
WHERE posts.poster = :user
  AND users.name = posts.poster
  AND posts.removed = FALSE
  AND users.removed = FALSE
ORDER BY posts.created
LIMIT :page_size OFFSET :page * :page_size;
