(ns reddit-clone.post.core
  (:require [reddit-clone.post.db :as db]
            [reddit-clone.middleware :as mw]
            [reddit-clone.util :as util]
            [reitit.coercion.malli :refer [coercion]]
            [ring.util.response :as res]
            [reddit-clone.comment.core :as comments]
            [reddit-clone.vote.core :as vote]
            [clojure.set :refer [join]]))

(defn create-post! [{:keys [db] :as req}]
  (let [id (util/gen-id 10)
        {:keys [subreddit content title]} (-> req :body-params)]
    (db/insert-post db {:post_id   id, :title title
                        :poster    (-> req :identity :user)
                        :subreddit subreddit, :content content})
    (res/created (str "/posts/" id))))

(defn remove-post!
  "Flag the post as removed, hiding it from all feeds and
  displaying the contents as \"Removed\" to everybody. Original
  data is preserved in db and can be queried manually."
  [{:keys [db] :as req}]
  (util/with-sql-try
    (let [post-id (-> req :path-params :post-id)
          user (-> req :identity :user)
          post-owner (:poster (db/get-post-info db {:name post-id}))]
      (if (= user post-owner)
        (do (db/update-post-removed db {:post_id post-id, :removed true})
            (res/status 204))
        (res/status 403)))))

(defn get-post [{:keys [db] :as req}]
  (res/response (db/get-post-info
                  db
                  {:name (-> req :path-params :post-id)})))

(defn get-subreddit-posts
  "Gets posts for the subreddit. Posts are
  descending sorted with depreciated vote,
  see vote.core ns for details. Results are
  paginated."
  [{:keys [db] :as req}]
  (let [posts (db/get-subreddit-posts
                db
                (merge {:subreddit (-> req
                                       :path-params
                                       :subreddit)}
                       (util/extract-pagination req)))
        votes (vote/get-posts-votes db posts)]
    (res/response (join (set posts) (set votes)))))

(defn get-user-posts
  "Gets feed of posts made by user. Results are
  paginated."
  [{:keys [db] :as req}]
  (let [posts (db/get-user-posts
                db
                (merge {:user (-> req :path-params :username)}
                       (util/extract-pagination req)))
        votes (vote/get-posts-votes db posts)]
    (res/response (join (set posts) (set votes)))))

(defn routes []
  ["" {:swagger {:tags ["Posts"]}, :coercion coercion}
   ["/posts"
    ["" {:post {:handler    create-post!
                :middleware [mw/wrap-subreddit-exists
                             mw/wrap-token mw/wrap-auth]
                :parameters {:body   [:map
                                      [:content [:re util/body-regex]]
                                      [:subreddit [:re util/subreddit-regex]]
                                      [:title [:re util/title-regex]]]
                             :header util/auth-header}}}]

    ["/:post-id" {:parameters {:path [:map [:post-id [:re util/id-regex]]]}
                  :middleware [mw/wrap-post-exists]}
     ["" {:delete {:handler    remove-post!
                   :middleware [mw/wrap-token mw/wrap-auth]
                   :parameters {:header util/auth-header}}
          :get    {:handler get-post}}]

     ["/comments" {:get {:handler comments/get-post-comments}}]

     ["/vote" {:middleware [[mw/wrap-vote-type :post]
                            mw/wrap-token
                            mw/wrap-auth]
               :parameters {:header util/auth-header}}
      ["" {:post   {:handler    vote/vote!
                    :parameters {:body vote/vote-params}}
           :delete {:handler vote/remove-vote!}}]]]]])
