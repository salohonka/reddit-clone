(ns reddit-clone.post.db
  (:require [hugsql.core :as sql]
            [hugsql-adapter-case.adapters :refer [kebab-adapter]]))

(sql/def-db-fns "reddit_clone/post/queries.sql" {:adapter (kebab-adapter)})
