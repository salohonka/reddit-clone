(ns reddit-clone.subreddit.db
  (:require [hugsql.core :as sql]
            [hugsql-adapter-case.adapters :refer [kebab-adapter]]))

(sql/def-db-fns "reddit_clone/subreddit/queries.sql" {:adapter (kebab-adapter)})
