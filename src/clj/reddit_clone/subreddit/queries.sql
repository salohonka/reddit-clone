-- :name get-subreddit-info :? :1
-- :doc Gets subreddit owner and flags
SELECT (owner, quarantined, private, removed, banned)
FROM subreddits
WHERE name = :name;

-- :name get-subreddit-owner :? :1
SELECT OWNER
FROM subreddits
WHERE NAME = :name;

-- :name insert-subreddit :! :n
INSERT
INTO subreddits (name, owner)
VALUES (:name, :owner);

-- :name update-subreddit-flag :! :n
-- :doc Sets specified boolean flag.
UPDATE subreddits
SET :i:flag = :value
WHERE name = :name;

-- :name check-subreddit-flag :? :1
-- :doc Returns specified boolean flag.
SELECT :flag
FROM subreddits
WHERE name = :name;

-- :name drop-subreddits :! :n
-- :doc Removes all subreddits from table for testing purposes
TRUNCATE subreddits CASCADE;
