(ns reddit-clone.subreddit.core
  (:require [reddit-clone.subreddit.db :as db]
            [reddit-clone.middleware :as mw]
            [reddit-clone.util :as util]
            [reitit.coercion.malli :refer [coercion]]
            [ring.util.response :as res]
            [reddit-clone.post.core :as post])
  (:import (java.sql SQLException)))

(defn- get-subreddit-info [{:keys [db] :as req}]
  (res/response
    (db/get-subreddit-info db {:name (-> req :path-params :subreddit)})))

(defn- create-subreddit! [{db :db, {:keys [subreddit]} :body-params, :as req}]
  (try
    (db/insert-subreddit db {:name  subreddit
                             :owner (-> req :identity :user)})
    (res/created (str "/subreddits/" subreddit))
    (catch SQLException _
      (res/bad-request {:message "Subreddit already exists."}))))

(defn- remove-subreddit!
  "Flag the subreddit as removed. Only admins can
  access removed subreddits, and the action is
  irreversible."
  [{db :db, {subreddit :subreddit} :path-params, :as req}]
  (util/with-sql-try
    (let [requester (-> req :identity :user)
          owner (:owner (db/get-subreddit-owner db {:name subreddit}))]
      (cond
        (not= requester owner) (res/status 403)

        (= 1 (db/update-subreddit-flag db {:name  subreddit
                                           :flag  "removed"
                                           :value true}))
        (res/status 204)

        :else (res/status 400)))))

(defn routes []
  ["" {:swagger {:tags ["Subreddits"]}, :coercion coercion}
   ["/subreddits"
    ["" {:post {:handler    create-subreddit!
                :parameters {:header util/auth-header
                             :body   [:map [:subreddit
                                            [:re util/subreddit-regex]]]}
                :middleware [mw/wrap-token mw/wrap-auth]}}]

    ["/:subreddit"
     {:parameters {:path [:map [:subreddit [:re util/subreddit-regex]]]}
      :middleware [mw/wrap-subreddit-exists]}

     ["" {:delete {:handler    remove-subreddit!
                   :parameters {:header util/auth-header}
                   :middleware [mw/wrap-token mw/wrap-auth]}
          :get    {:handler get-subreddit-info}}]

     ["/posts" {:get (util/create-paged-endpoint
                       post/get-subreddit-posts)}]]]])
