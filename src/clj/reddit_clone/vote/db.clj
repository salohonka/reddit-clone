(ns reddit-clone.vote.db
  (:require [hugsql.core :as sql]
            [hugsql-adapter-case.adapters :refer [kebab-adapter]]))

(sql/def-db-fns "reddit_clone/vote/queries.sql" {:adapter (kebab-adapter)})
