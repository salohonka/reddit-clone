-- :name drop-comment-votes :! :n
-- :doc Removes all comment votes from table for testing purposes
TRUNCATE comment_votes CASCADE;

-- :name drop-post-votes :! :n
-- :doc Removes all post votes from table for testing purposes
TRUNCATE post_votes CASCADE;

-- :name upsert-post-vote :! :n
-- :doc Insert or update a post vote
INSERT INTO post_votes (owner, post, upvote)
VALUES (:owner, :post_id, :upvote?)
ON CONFLICT (owner, post)
    DO UPDATE SET upvote = :upvote?;

-- :name upsert-comment-vote :! :n
-- :doc Insert or update a comment vote
INSERT INTO comment_votes (owner, comment, upvote)
VALUES (:owner, :comment_id, :upvote?)
ON CONFLICT (owner, comment)
    DO UPDATE SET upvote = :upvote?;

-- :name delete-post-vote :! :n
DELETE
FROM post_votes
WHERE owner = :owner
  AND post = :post_id;

-- :name delete-comment-vote :! :n
DELETE
FROM comment_votes
WHERE owner = :owner
  AND comment = :comment_id;

-- :name get-user-votes-for-posts-array :? :*
/* :doc
   Get vote statuses for given array of post ids.
   Used for getting info which posts user has voted
   on when displaying a list of posts.

   Beware of autoformat breaking the hugsql list-value-type!
 */
SELECT post, upvote
FROM post_votes
WHERE post IN (:v*:post_ids)
  AND owner = :owner;

-- :name get-user-votes-for-comments-array :? :*
/* :doc
   Get vote statuses for given array of comment ids.
   Used for getting info which posts user has voted
   on when displaying a list of comments.

   Beware of autoformat breaking the hugsql list-value-type!
 */
SELECT comment, upvote
FROM comment_votes
WHERE comment IN (:v*:comment_ids)
  AND owner = :owner;

-- :name get-vote-sum-for-posts-array :? :*
/* :doc
   Get count of (upvotes - downvotes) for given posts.

   Beware of autoformat breaking the hugsql list-value-type!
 */
SELECT COUNT(*) FILTER (WHERE upvote = TRUE) -
       COUNT(*) FILTER (WHERE upvote = FALSE) AS votes,
       post as post_id
FROM post_votes
WHERE post IN (:v*:post_ids)
GROUP BY post_id;

-- :name get-vote-sum-for-comments-array :? :*
/* :doc
   Get count of (upvotes - downvotes) for given comments.

   Beware of autoformat breaking the hugsql list-value-type!
 */
SELECT COUNT(*) FILTER (WHERE upvote = TRUE) -
       COUNT(*) FILTER (WHERE upvote = FALSE) AS votes,
       comment AS comment_id
FROM comment_votes
WHERE comment IN (:v*:comment_ids)
GROUP BY comment_id;
