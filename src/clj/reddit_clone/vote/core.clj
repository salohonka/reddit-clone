(ns reddit-clone.vote.core
  (:require [ring.util.response :as res]
            [reddit-clone.vote.db :as db]
            [reddit-clone.util :as util]
            [reitit.coercion.malli :refer [coercion]]
            [clojure.set :refer [difference]]
            [reddit-clone.middleware :as mw]))

(def display-asymptote-target
  "Asymptotic target towards with display
  upvote count will grow, getting slower
  and slower and ultimately never reaching."
  20000)

(def display-asymptote-growth-rate
  "Rate at which asymptotic upvote count will
  grow. Higher number means slower growth."
  20000)

(defn calculate-display-votes
  "Calculates the display value of votes
  for a given entity. Total vote amount
  follows a logarithmic curve to even out
  extreme vote amounts."
  [vote-count]
  (if (zero? vote-count)
    0
    (cond-> (/ (*' display-asymptote-target
                   (Math/pow vote-count 2))
               (+' (Math/pow vote-count 2)
                   (*' (Math/abs ^int vote-count)
                       display-asymptote-growth-rate)))
            true Math/ceil
            true int
            (neg? vote-count) -)))

(defn calculate-depreciated-votes
  "Calculates the sort value of votes that
  depreciates over time. This creates churn
  in the content visible to the user."
  [created vote-count]
  (rand-int 200))

(def vote-params [:map [:upvote boolean?]])

(defmulti vote! :vote-type)

(defmethod vote! :post [req]
  (db/upsert-post-vote
    (:db req)
    {:owner   (-> req :identity :user)
     :post_id (-> req :path-params :post-id)
     :upvote? (-> req :body-params :upvote)})
  (res/status 204))

(defmethod vote! :comment [req]
  (db/upsert-comment-vote
    (:db req)
    {:owner      (-> req :identity :user)
     :comment_id (-> req :path-params :comment-id)
     :upvote?    (-> req :body-params :upvote)})
  (res/status 204))

(defmulti remove-vote! :vote-type)

(defmethod remove-vote! :post [req]
  (util/with-sql-try
    (do (db/delete-post-vote
          (:db req)
          {:owner   (-> req :identity :user)
           :post_id (-> req :path-params :post-id)})
        (res/status 204))))

(defmethod remove-vote! :comment [req]
  (util/with-sql-try
    (do (db/delete-comment-vote
          (:db req)
          {:owner      (-> req :identity :user)
           :comment_id (-> req :path-params :comment-id)})
        (res/status 204))))

(defn get-user-votes-for-posts
  "Get information whether user has voted or not
  on given posts."
  [{:keys [db] :as req}]
  (db/get-user-votes-for-posts-array
    db
    {:post_ids (-> req :query-params :post-ids)
     :owner    (-> req :identity :user)}))

(defn get-user-votes-for-comments
  "Get information whether user has voted or not
  on given comments."
  [{:keys [db] :as req}]
  (db/get-user-votes-for-comments-array
    db
    {:comment_ids (-> req :query-params :comment-ids)
     :owner       (-> req :identity :user)}))

(defn get-posts-votes
  "Get vote counts/score for seq of posts."
  [db posts]
  (when (seq posts)
    (let [ids (map :post-id posts)
          found-votes (db/get-vote-sum-for-posts-array
                        db
                        {:post_ids ids})]
      (as-> (set ids) $
            (difference $ (set (map :post-id found-votes)))
            (map (fn [id] {:post-id id, :votes 0}) $)
            (into found-votes $)
            (map #(update % :votes calculate-display-votes) $)))))

(defn get-comments-votes
  "Get vote counts/score for seq of posts."
  [db comments]
  (when (seq comments)
    (let [ids (map :comment-id comments)
          found-votes (db/get-vote-sum-for-comments-array
                        db
                        {:comment_ids ids})]
      (as-> (set ids) $
            (difference $ (set (map :comment-id found-votes)))
            (map (fn [id] {:comment-id id, :votes 0}) $)
            (into found-votes $)
            (map #(update % :votes calculate-display-votes) $)))))

(defn routes []
  ["" {:swagger {:tags ["Votes"]}, :coercion coercion}
   ["/votes" {:middleware [mw/wrap-token mw/wrap-auth]
              :parameters {:header util/auth-header}}
    ["/comments" {:get {:handler    get-user-votes-for-posts
                        :parameters {:query
                                     [:map
                                      [:comment-ids
                                       [:vector
                                        [:re util/id-regex]]]]}}}]

    ["/posts" {:get {:handler    get-user-votes-for-comments
                     :parameters {:query
                                  [:map
                                   [:post-ids
                                    [:sequential
                                     [:re util/id-regex]]]]}}}]]])
