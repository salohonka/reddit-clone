(ns reddit-clone.moderation.core)

(defn add-moderator! [user subreddit]
  nil)

(defn remove-moderator! [user subreddit]
  nil)

(defn moderator? [user subreddit]
  nil)

(defn owner?
  "Is user creator of subreddit?"
  [user subreddit]
  nil)

(defn user-banned?
  "See `ban!`."
  [user subreddit]
  nil)

(defn set-post-lock!
  "Set the lock-status of the post. Only moderators and
  admins can comment or vote on locked posts and their
  comments."
  [post locked?]
  nil)

(defn set-lock-status!
  "Set the lock-status of the subreddit. Only moderators
  and admins can access locked subreddits. All other users
  are shown a warning that the subreddit is locked."
  [subreddit locked?]
  nil)

(defn set-private-status!
  "Set the private-status of the subreddit. Only invited
  members, moderators and admins can access private
  subreddits. All other users are shown a warning that
  the subreddit is private."
  [subreddit private?]
  nil)

(defn ban!
  "Bans the user from the subreddit. Ban duration in days
  can be specified in opts-map, defaults to 7 days. Banned
  users cannot post, comment or vote on the subreddit."
  ([user subreddit]
   (ban! user subreddit {:moderation/ban-days 7}))

  ([user subreddit {:moderation/keys [ban-days]}]
   nil))

(defn unban!
  "See `ban!`."
  [user]
  nil)

(defn remove-entity!
  "Remove a post or comment made in the subreddit.
  Removed posts are hidden from feeds, and removed
  comments are marked as \"Removed\"."
  [entity subreddit]
  nil)
