(ns reddit-clone.admin.core)

(defn is-admin? [user]
  nil)

(defn ban-subreddit!
  "Bans the subreddit. Only admins can access
  banned subreddits. Subreddit bans are indefinite
  by default, and must always be manually undone."
  [subreddit]
  nil)

(defn unban-subreddit!
  "See `ban-subreddit!`."
  [subreddit]
  nil)

(defn set-subreddit-quarantine!
  "Sets the subreddits quarantine status.
  Quarantined subreddits display a warning to
  users who are navigating to the subreddit, but
  does not prevent any other actions."
  [subreddit quarantined?]
  nil)

(defn shadowban!
  "Shadowbans the user, allowing only subreddit
  moderators and admins to see their posts and
  comments. Shadowbans are indefinite by default,
  and must always be manually undone."
  [user]
  nil)

(defn unshadowban!
  "See `shadowban!`."
  [user]
  nil)
