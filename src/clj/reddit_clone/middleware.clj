(ns reddit-clone.middleware
  (:require [reddit-clone.auth :as auth]
            [buddy.auth.middleware :as buddy-mw]
            [buddy.auth :as buddy-auth]
            [ring.util.response :as responses]
            [reitit.ring.middleware.parameters :as parameters]
            [reitit.ring.middleware.exception :as exception]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.coercion :as coercion]
            [reitit.swagger :as swagger]
            [reddit-clone.subreddit.db :as subreddit-db]
            [reddit-clone.post.db :as post-db]
            [reddit-clone.user.db :as user-db]
            [reddit-clone.comment.db :as comment-db]))

(defn wrap-token
  "Extracts JWT payload from auth header
  and injects into request map under :identity."
  [handler]
  (buddy-mw/wrap-authentication handler auth/buddy-backend))

(defn wrap-auth
  "Checks for successful authentication.
  Returns 403 response if not found."
  [handler]
  (fn [req]
    (if (buddy-auth/authenticated? req)
      (handler req)
      (responses/status 403))))

(defn wrap-db [handler db]
  (fn [req] (handler (assoc req :db (:pool db)))))

(defn wrap-comment-author
  "Checks for identity to match original
  comment author. If not author, returns 403."
  [handler]
  (fn [{:keys [db] :as req}]
    (let [claimant (-> req :identity :user)]
      (if (= (:poster (comment-db/get-comment-info
                        db
                        {:name (-> req
                                   :path-params
                                   :comment-id)}))
             claimant)
        (handler req)
        (responses/status 403)))))

(def entity-defs
  "Mapping of domain entity to parameter
  keyword and database function to check
  existence for `wrap-x-exists` mws."
  {:subreddit {:kw       :subreddit
               :fetch-fn subreddit-db/get-subreddit-info}
   :post      {:kw       :post-id
               :fetch-fn post-db/get-post-info}
   :user      {:kw       :username
               :fetch-fn user-db/get-user-info}
   :comment   {:kw       :comment-id
               :fetch-fn comment-db/get-comment-info}})

(defn- wrap-x-exists
  "Creates a generic 400/404 middleware
  for detecting missing domain objects
  in path or body params."
  [entity-kw]
  (let [{:keys [kw fetch-fn]} (get entity-defs entity-kw)]
    (fn [handler]
      (fn [req]
        (let [path-param (-> req :path-params kw)]
          (if (fetch-fn (:db req)
                        {:name (if path-param
                                 path-param
                                 (-> req :body-params kw))})
            (handler req)
            (responses/status (if path-param 404 400))))))))

(def wrap-subreddit-exists
  "Checks if subreddit exists,
  if not then returns status 404."
  (wrap-x-exists :subreddit))

(def wrap-post-exists
  "Checks if post exists,
  if not then returns status 404."
  (wrap-x-exists :post))

(def wrap-user-exists
  "Checks if user exists,
  if not then returns status 404."
  (wrap-x-exists :user))

(def wrap-comment-exists
  "Checks if comment exists,
  if not then returns status 404."
  (wrap-x-exists :comment))

(defn wrap-vote-type
  "Assocs :vote-type key into request
  with a value of :comment or :post."
  [handler type]
  (fn [req]
    (handler (assoc req :vote-type type))))

(def default-middleware [swagger/swagger-feature
                         parameters/parameters-middleware
                         muuntaja/format-middleware
                         exception/exception-middleware
                         coercion/coerce-request-middleware])
