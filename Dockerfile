FROM clojure:lein-alpine as build-stage

RUN mkdir /reddit
WORKDIR /reddit
COPY . .
COPY profiles.example.clj profiles.clj
RUN ["lein","uberjar"]

FROM openjdk:11-jre-slim

RUN mkdir /reddit
WORKDIR /reddit
COPY --from=build-stage /reddit/target/uberjar/reddit-clone.jar reddit-clone.jar
RUN chmod a+x reddit-clone.jar && \
    useradd -m reddit
ENV DATABASE_URL=db \
    DATABASE_NAME=reddit \
    DATABASE_PORT=5432 \
    POSTGRES_USER=reddit \
    PORT=3000
USER reddit
CMD ["java","-jar","reddit-clone.jar"]
